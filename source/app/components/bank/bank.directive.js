import { bankController } from './bank.controller';

export function bankDirective() {
  const directive = {
    templateUrl: 'app/components/bank/bank.view.html',
    restrict: 'EA',
    replace: true,
    controller: bankController,
    controllerAs: 'vm',
    bindToController: true,
  };
  return directive;
}
