export function lobbyController(players, $state) {
  const vm = this;
  vm.users = players.users;
  vm.name = '';
  function addUser(name = '') {
    players.addUser({ money: 15000, name });
    vm.name = '';
  }
  function deleteUser(id) {
    players.deleteUser(id);
  }
  function startGame() {
    $state.go('bank');
  }
  vm.addUser = addUser;
  vm.deleteUser = deleteUser;
  this.startGame = startGame;
}

lobbyController.$inject = ['players', '$state'];
