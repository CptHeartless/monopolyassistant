import { lobbyController } from './lobby.controller';

export function lobbyDirective() {
  const directive = {
    templateUrl: 'app/components/lobby/lobby.view.html',
    restrict: 'EA',
    replace: true,
    controller: lobbyController,
    controllerAs: 'vm',
    bindToController: true,
  };
  return directive;
}
