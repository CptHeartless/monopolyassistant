export function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
  /* @ngInject */
  function RouterHelper($state) {
    let hasOtherwise = false;
    return {
      configureStates(states, otherwisePath) {
        states.forEach((state) => {
          $stateProvider.state(state.state, state.config);
        });
        if (otherwisePath && !hasOtherwise) {
          hasOtherwise = true;
          $urlRouterProvider.otherwise(otherwisePath);
        }
      },
      getStates() {
        return $state.get();
      },
    };
  }
  RouterHelper.$inject = ['$state'];

  this.$get = RouterHelper;
  $locationProvider.html5Mode(false).hashPrefix('!');
}
routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
