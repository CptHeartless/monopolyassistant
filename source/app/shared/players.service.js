import { playersIcons } from './players.icons';
import { playersColors } from './players.colors';

export function playersService() {
  this.users = [];
  function getIcon() {
    if (!this.icons || !this.icons.length) this.icons = [...playersIcons];
    const iconNumber = Math.floor(Math.random() * this.icons.length);
    return this.icons.splice(iconNumber, 1)[0];
  }

  function getColor() {
    if (!this.colors || !this.colors.length) this.colors = [...playersColors];
    const colorNumber = Math.floor(Math.random() * this.colors.length);
    return this.colors.splice(colorNumber, 1)[0];
  }

  function addUser({ name = '', money = 0 }) {
    if (name.length === 0) return false;
    const icon = this.getIcon();
    const color = this.getColor();
    const id = this.users.length;
    this.users.push({ name, money, id, icon, color });
    return true;
  }

  function decreaseMoney(id, amount) {
    if (!this.users[id]) return false;
    this.users[id].money -= Number(amount);
    return true;
  }

  function increaseMoney(id, amount) {
    if (!this.users[id]) return false;
    this.users[id].money += Number(amount);
    return true;
  }

  function deleteUser(id) {
    const user = this.users[id];
    this.colors.push(user.color);
    this.icons.push(user.icon);
    this.users.splice(id, 1);
  }

  function getUsers() {
    return this.users;
  }
  this.getColor = getColor;
  this.getIcon = getIcon;
  this.addUser = addUser;
  this.deleteUser = deleteUser;
  this.getUsers = getUsers;
  this.decreaseMoney = decreaseMoney;
  this.increaseMoney = increaseMoney;
}
