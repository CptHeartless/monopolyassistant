import 'angular';
import 'monopoly.module';
import 'monopoly.routes';

angular.element(document).ready(() => {
  angular.bootstrap(document.body, ['monopoly']);
});
