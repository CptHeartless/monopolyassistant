function getStates() {
  return [
    {
      state: 'lobby',
      config: {
        template: '<lobby />',
        url: '',
      },
    },
    {
      state: 'bank',
      config: {
        template: '<bank />',
        url: '/bank',
      }
    }
  ];
}

function appRun(routerHelper) {
  routerHelper.configureStates(getStates());
}
appRun.$inject = ['routerHelper'];

angular
  .module('monopoly')
    .run(appRun);
