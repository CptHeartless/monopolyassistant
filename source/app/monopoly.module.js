import 'angular-ui-router';
import { playersService } from './shared/players.service';
import { lobbyDirective } from './components/lobby/lobby.directive';
import { bankDirective } from './components/bank/bank.directive';
import { routerHelperProvider } from './shared/routerHelper.provider';

angular
  .module('monopoly', ['ui.router'])
    .provider('routerHelper', routerHelperProvider)
    .service('players', playersService)
    .directive('lobby', lobbyDirective)
    .directive('bank', bankDirective);
