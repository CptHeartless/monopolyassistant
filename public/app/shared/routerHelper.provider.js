'use strict';

System.register([], function (_export, _context) {
  return {
    setters: [],
    execute: function () {
      function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
        /* @ngInject */
        function RouterHelper($state) {
          var hasOtherwise = false;
          return {
            configureStates: function configureStates(states, otherwisePath) {
              states.forEach(function (state) {
                $stateProvider.state(state.state, state.config);
              });
              if (otherwisePath && !hasOtherwise) {
                hasOtherwise = true;
                $urlRouterProvider.otherwise(otherwisePath);
              }
            },
            getStates: function getStates() {
              return $state.get();
            }
          };
        }
        RouterHelper.$inject = ['$state'];

        this.$get = RouterHelper;
        $locationProvider.html5Mode(false).hashPrefix('!');
      }

      _export('routerHelperProvider', routerHelperProvider);

      routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
    }
  };
});
//# sourceMappingURL=routerHelper.provider.js.map
