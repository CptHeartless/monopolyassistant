'use strict';

System.register([], function (_export, _context) {
  var playersIcons;
  return {
    setters: [],
    execute: function () {
      _export('playersIcons', playersIcons = ['alligator', 'anteater', 'armadillo', 'auroch', 'axolotl', 'badger', 'bat', 'beaver', 'buffalo', 'camel', 'chameleon', 'chipmunk', 'chinchilla', 'chupacabra', 'cormorant', 'coyote', 'crow', 'dinosaur', 'dolphin', 'duck', 'elephant', 'ferret', 'fox', 'frog', 'giraffe', 'gopher', 'grizzly', 'hedgehog', 'hippo', 'hyena', 'jackal', 'ibex', 'ifrit', 'iguana', 'koala', 'kraken', 'lemur', 'leopard', 'liger', 'llama', 'manatee', 'mink', 'monkey', 'narwhal', 'nyancat', 'orangutan', 'otter', 'panda', 'penguin', 'platypus', 'python', 'pumpkin', 'quagga', 'rabbit', 'raccoon', 'rhino', 'sheep', 'shrew', 'skunk', 'slowloris', 'squirrel', 'turtle', 'walrus', 'wolf', 'wolverine', 'wombat', 'tiger', 'moose', 'dingo', 'cheetah', 'capybara']);

      _export('playersIcons', playersIcons);
    }
  };
});
//# sourceMappingURL=players.icons.js.map
