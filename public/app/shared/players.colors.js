'use strict';

System.register([], function (_export, _context) {
  var playersColors;
  return {
    setters: [],
    execute: function () {
      _export('playersColors', playersColors = ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'deep-orange', 'brown', 'blue-grey', 'lime', 'grey']);

      _export('playersColors', playersColors);
    }
  };
});
//# sourceMappingURL=players.colors.js.map
