'use strict';

System.register(['./players.icons', './players.colors'], function (_export, _context) {
  var playersIcons, playersColors;

  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }

      return arr2;
    } else {
      return Array.from(arr);
    }
  }

  return {
    setters: [function (_playersIcons) {
      playersIcons = _playersIcons.playersIcons;
    }, function (_playersColors) {
      playersColors = _playersColors.playersColors;
    }],
    execute: function () {
      function playersService() {
        this.users = [];
        function getIcon() {
          if (!this.icons || !this.icons.length) this.icons = [].concat(_toConsumableArray(playersIcons));
          var iconNumber = Math.floor(Math.random() * this.icons.length);
          return this.icons.splice(iconNumber, 1)[0];
        }

        function getColor() {
          if (!this.colors || !this.colors.length) this.colors = [].concat(_toConsumableArray(playersColors));
          var colorNumber = Math.floor(Math.random() * this.colors.length);
          return this.colors.splice(colorNumber, 1)[0];
        }

        function addUser(_ref) {
          var _ref$name = _ref.name;
          var name = _ref$name === undefined ? '' : _ref$name;
          var _ref$money = _ref.money;
          var money = _ref$money === undefined ? 0 : _ref$money;

          if (name.length === 0) return false;
          var icon = this.getIcon();
          var color = this.getColor();
          var id = this.users.length;
          this.users.push({ name: name, money: money, id: id, icon: icon, color: color });
          return true;
        }

        function decreaseMoney(id, amount) {
          if (!this.users[id]) return false;
          this.users[id].money -= Number(amount);
          return true;
        }

        function increaseMoney(id, amount) {
          if (!this.users[id]) return false;
          this.users[id].money += Number(amount);
          return true;
        }

        function deleteUser(id) {
          var user = this.users[id];
          this.colors.push(user.color);
          this.icons.push(user.icon);
          this.users.splice(id, 1);
        }

        function getUsers() {
          return this.users;
        }
        this.getColor = getColor;
        this.getIcon = getIcon;
        this.addUser = addUser;
        this.deleteUser = deleteUser;
        this.getUsers = getUsers;
        this.decreaseMoney = decreaseMoney;
        this.increaseMoney = increaseMoney;
      }

      _export('playersService', playersService);
    }
  };
});
//# sourceMappingURL=players.service.js.map
