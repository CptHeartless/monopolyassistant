'use strict';

System.register(['angular-ui-router', './shared/players.service', './components/lobby/lobby.directive', './components/bank/bank.directive', './shared/routerHelper.provider'], function (_export, _context) {
  var playersService, lobbyDirective, bankDirective, routerHelperProvider;
  return {
    setters: [function (_angularUiRouter) {}, function (_sharedPlayersService) {
      playersService = _sharedPlayersService.playersService;
    }, function (_componentsLobbyLobbyDirective) {
      lobbyDirective = _componentsLobbyLobbyDirective.lobbyDirective;
    }, function (_componentsBankBankDirective) {
      bankDirective = _componentsBankBankDirective.bankDirective;
    }, function (_sharedRouterHelperProvider) {
      routerHelperProvider = _sharedRouterHelperProvider.routerHelperProvider;
    }],
    execute: function () {

      angular.module('monopoly', ['ui.router']).provider('routerHelper', routerHelperProvider).service('players', playersService).directive('lobby', lobbyDirective).directive('bank', bankDirective);
    }
  };
});
//# sourceMappingURL=monopoly.module.js.map
