'use strict';

System.register([], function (_export, _context) {
  return {
    setters: [],
    execute: function () {
      function bankController(players) {
        var vm = this;
        vm.users = players.users;
        vm.amount = '';
        vm.transfer = { last: true };

        function transferReverse() {
          var buff = vm.transfer.to;
          vm.transfer.to = vm.transfer.from;
          vm.transfer.from = buff;
        }

        function clearAmount() {
          vm.amount = '';
        }

        function addToTransfer(player) {
          if (vm.transfer.last || vm.transfer.hasOwnProperty('from') && vm.transfer.from.id === player.id) {
            vm.transfer.from = player;
            vm.transfer.last = false;
          } else if (vm.transfer.hasOwnProperty('to') && vm.transfer.to.id === player.id || !vm.transfer.last) {
            vm.transfer.to = player;
            vm.transfer.last = true;
          }
          return true;
        }

        function removeFromTransfer(direction) {
          if (vm.transfer.hasOwnProperty(direction) && direction === 'last') return false;
          delete vm.transfer[direction];
          return true;
        }

        function doTransfer() /* modificator */{
          if (!vm.amount) return false;
          if (vm.transfer.from !== undefined) players.decreaseMoney(vm.transfer.from.id, vm.amount);
          if (vm.transfer.to !== undefined) players.increaseMoney(vm.transfer.to.id, vm.amount);
          return true;
        }

        function addToAmount(number) {
          if (!vm.amount) vm.amount = '';
          var value = String(number).match(/[0-9\.]*/)[0] || [false];
          if (value === false) return value;
          if (vm.amount === '0') vm.amount = '';
          vm.amount = vm.amount + value;
          return true;
        }

        vm.clearAmount = clearAmount;
        vm.transfer.reverse = transferReverse;
        vm.removeFromTransfer = removeFromTransfer;
        vm.addToTransfer = addToTransfer;
        vm.addToAmount = addToAmount;
        vm.doTransfer = doTransfer;
      }

      _export('bankController', bankController);

      bankController.$inject = ['players'];
    }
  };
});
//# sourceMappingURL=bank.controller.js.map
