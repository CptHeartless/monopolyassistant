'use strict';

System.register(['./bank.controller'], function (_export, _context) {
  var bankController;
  return {
    setters: [function (_bankController) {
      bankController = _bankController.bankController;
    }],
    execute: function () {
      function bankDirective() {
        var directive = {
          templateUrl: 'app/components/bank/bank.view.html',
          restrict: 'EA',
          replace: true,
          controller: bankController,
          controllerAs: 'vm',
          bindToController: true
        };
        return directive;
      }

      _export('bankDirective', bankDirective);
    }
  };
});
//# sourceMappingURL=bank.directive.js.map
