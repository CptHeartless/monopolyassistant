'use strict';

System.register([], function (_export, _context) {
  return {
    setters: [],
    execute: function () {
      function lobbyController(players, $state) {
        var vm = this;
        vm.users = players.users;
        vm.name = '';
        function addUser() {
          var name = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];

          players.addUser({ money: 15000, name: name });
          vm.name = '';
        }
        function deleteUser(id) {
          players.deleteUser(id);
        }
        function startGame() {
          $state.go('bank');
        }
        vm.addUser = addUser;
        vm.deleteUser = deleteUser;
        this.startGame = startGame;
      }

      _export('lobbyController', lobbyController);

      lobbyController.$inject = ['players', '$state'];
    }
  };
});
//# sourceMappingURL=lobby.controller.js.map
