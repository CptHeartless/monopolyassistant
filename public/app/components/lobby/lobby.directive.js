'use strict';

System.register(['./lobby.controller'], function (_export, _context) {
  var lobbyController;
  return {
    setters: [function (_lobbyController) {
      lobbyController = _lobbyController.lobbyController;
    }],
    execute: function () {
      function lobbyDirective() {
        var directive = {
          templateUrl: 'app/components/lobby/lobby.view.html',
          restrict: 'EA',
          replace: true,
          controller: lobbyController,
          controllerAs: 'vm',
          bindToController: true
        };
        return directive;
      }

      _export('lobbyDirective', lobbyDirective);
    }
  };
});
//# sourceMappingURL=lobby.directive.js.map
