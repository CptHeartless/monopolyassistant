'use strict';

System.register([], function (_export, _context) {
  function getStates() {
    return [{
      state: 'lobby',
      config: {
        template: '<lobby />',
        url: ''
      }
    }, {
      state: 'bank',
      config: {
        template: '<bank />',
        url: '/bank'
      }
    }];
  }

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }
  return {
    setters: [],
    execute: function () {
      appRun.$inject = ['routerHelper'];

      angular.module('monopoly').run(appRun);
    }
  };
});
//# sourceMappingURL=monopoly.routes.js.map
