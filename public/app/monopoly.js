'use strict';

System.register(['angular', 'monopoly.module', 'monopoly.routes'], function (_export, _context) {
  return {
    setters: [function (_angular) {}, function (_monopolyModule) {}, function (_monopolyRoutes) {}],
    execute: function () {

      angular.element(document).ready(function () {
        angular.bootstrap(document.body, ['monopoly']);
      });
    }
  };
});
//# sourceMappingURL=monopoly.js.map
