const gulp = require('gulp');
const jade = require('gulp-jade');
const watch = require('gulp-watch');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');
const spritesmith = require('gulp.spritesmith');
const reload = browserSync.reload;

const settings = {
  templates: {
    output: 'public/',
    input: ['source/.jade', 'source/**/*.jade', '!source/**/_*.jade'],
    watch: ['source/.jade', 'source/**/*.jade', '!source/**/_*.jade'],
  },
  styles: {
    output: 'public/assets/styles/',
    input: 'source/assets/styles/*.scss',
    sourcemaps: '.',
    watch: ['source/assets/styles/*.scss', 'source/assets/styles/**/*.scss'],
  },
  scripts: {
    output: 'public/app/',
    input: ['source/app/*.js', 'source/app/**/*.js'],
    sourcemaps: '.',
    watch: ['source/app/*.js', 'source/app/**/*.js'],
  },
  fonts: {
    output: 'public/assets/fonts/',
    input: ['source/assets/fonts/*.*', 'source/assets/fonts/**/*.*'],
    watch: ['source/assets/fonts/*.*', 'source/assets/fonts/**/*.*'],
  },
  vendors: {
    output: 'public/assets/vendors/',
    input: 'source/assets/vendors/**/*.*',
    watch: 'source/assets/vendors/**/*.*',
  },
  sprites: {
    input: 'source/assets/images/icons/*.*',
    output: 'public/assets/images/',
    cssPath: '../images/',
    watch: 'source/assets/images/icons/*.*',
    spriteSheetOutput: 'source/assets/styles/core/',
    spriteSheetName: '_spritesheet.scss',
    spriteMapName: 'spritemap.png',
  },
  webserver: {
    server: {
      baseDir: './public',
    },
    host: 'localhost',
    port: 8080,
  },
};

gulp.task('templates:build', () => {
  gulp.src(settings.templates.input)
    .pipe(jade({ pretty: true }))
    .pipe(gulp.dest(settings.templates.output))
    .pipe(reload({ stream: true }));
});

gulp.task('styles:build', () => {
  gulp.src(settings.styles.input)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write(settings.styles.sourcemaps))
    .pipe(gulp.dest(settings.styles.output))
    .pipe(reload({ stream: true }));
});

gulp.task('scripts:build', () => {
  gulp.src(settings.scripts.input)
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(sourcemaps.write(settings.scripts.sourcemaps))
    .pipe(gulp.dest(settings.scripts.output))
    .pipe(reload({ stream: true }));
});

gulp.task('vendors', () => {
  gulp.src(settings.vendors.input)
    .pipe(gulp.dest(settings.vendors.output))
    .pipe(reload({ stream: true }));
});

gulp.task('sprites:build', () => {
  const spriteData = gulp.src(settings.sprites.input)
    .pipe(spritesmith({
      imgName: settings.sprites.spriteMapName,
      cssName: settings.sprites.spriteSheetName,
      padding: 5,
      imgPath: settings.sprites.cssPath + settings.sprites.spriteMapName,
    }));
  spriteData.img
    .pipe(gulp.dest(settings.sprites.output));
  spriteData.css
    .pipe(gulp.dest(settings.sprites.spriteSheetOutput));
});

gulp.task('fonts', () => {
  gulp.src(settings.fonts.input)
    .pipe(gulp.dest(settings.fonts.output))
    .pipe(reload({ stream: true }));
});

gulp.task('watch', () => {
  watch(settings.templates.watch, () => {
    gulp.start('templates:build');
  });

  watch(settings.styles.watch, () => {
    gulp.start('styles:build');
  });

  watch(settings.scripts.watch, () => {
    gulp.start('scripts:build');
  });

  watch(settings.fonts.watch, () => {
    gulp.start('fonts');
  });

  watch(settings.vendors.watch, () => {
    gulp.start('vendors');
  });
});

gulp.task('webserver', () => {
  browserSync(settings.webserver);
});

gulp.task('build', [
  'styles:build',
  'templates:build',
  'scripts:build',
  'fonts',
  'vendors',
]);

gulp.task('default', ['build', 'webserver', 'watch']);
